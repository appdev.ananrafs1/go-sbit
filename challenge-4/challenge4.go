package challenge4

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type Solution struct{}

func (s Solution) Run() error {
	anagram, err := getStrings(`./challenge-4/anagramlist.json`)
	if err != nil {
		return err
	}
	return findAnagram(anagram)

}

func findAnagram(str arrString) error {
	fmt.Printf("list : %v\n", str)
	arr := make(arrString, 0)
	res := make([]arrString, 0)
	for i := 0; i < len(str)-1; i++ {
		for j := i + 1; j < len(str); j++ {
			if isAnagram(str[i], str[j]) {
				arr = append(arr, str[j])
				str.remove(j)
				j--
				continue
			}
		}
		//check if next is available
		if (i + 1) < len(str) {
			res = append(res, arr)
			arr = arrString{str[i+1]}

		}
	}
	res = append(res, arr)
	fmt.Println(res)
	return nil

}
func isAnagram(i, j string) bool {
	mapRune := make(map[rune]int, 0)
	for _, v := range i {
		mapRune[v] += 1
	}
	for _, v := range j {
		mapRune[v] += 1
	}
	for _, v := range mapRune {
		if v%2 != 0 {
			return false
		}
	}
	return true
}

type arrString []string

func (arr *arrString) remove(i int) {
	(*arr) = append((*arr)[:i], (*arr)[i+1:]...)
}

func getStrings(loc string) (arrString, error) {
	var res arrString
	fl, err := ioutil.ReadFile(loc)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(fl, &res)
	if err != nil {
		return nil, err
	}

	return res, nil
}
