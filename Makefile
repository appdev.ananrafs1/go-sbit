
sol1_clean:
	docker rm -f sol1db

sol1_rundb: sol1_clean
	docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=Qwertyuiop[]" --name sol1db -v "${CURDIR}/challenge-1/sql/Init.sql:/Init.sql" -p 1221:1433 -d mcr.microsoft.com/mssql/server:2019-latest && timeout 10
	docker exec -it --user root sol1db /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P Qwertyuiop[] -i Init.sql

sol1: sol1_rundb
	go run ./main.go -sol 1

sol3:
	go test -v ./challenge-3/challenge3_test.go

sol4:
	go run ./main.go -sol 4

must-have-%:
	@: $(if $(value $*),, $(error $* is undefined))

sol2_grpc_server : | must-have-omdbapi sol2_rundb 
		go run ./challenge-2/Delivery/grpc/server/main.go -omdbapi=$(omdbapi)

sol2_rest: | must-have-omdbapi sol2_rundb 
	go run ./challenge-2/Delivery/REST/main.go -omdbapi=$(omdbapi)

sol2_rundb: sol2_clean
	$ docker run --name sol2db -e MYSQL_ROOT_PASSWORD=Qwertyuiop[] -p 3306:3306 -v "${CURDIR}/challenge-2/sql/Init.sql:/Init.sql" -d mysql && timeout 15
	docker exec sol2db /bin/sh -c 'mysql -uroot -pQwertyuiop[] </Init.sql'

sol2_clean:
	docker rm -f sol2db

sol2_grpc_client: | must-define-operation
	go run ./challenge-2/Delivery/grpc/client/main.go -operation="$(operation)"


must-define-%:
	@: $(if $(value $*),, $(error $* is undefined, make sol2_grpc_client operation=1 | operation=0))