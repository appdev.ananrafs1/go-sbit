
## Challenge 1

Terdapat sebuah table "USER" yg memiliki 3 kolom: ID, UserName, Parent. Di mana:
Kolom ID adalah Primary Key
Kolom UserName adalah Nama User
Kolom Parent adalah ID dari User yang menjadi Creator untuk User tertentu.
eg.
```
——————————————————————————
| ID | UserName | Parent |
——————————————————————————
| 1 | Ali | 2 |
| 2 | Budi | 0 |
| 3 | Cecep | 1 |
—————————————————————————-
```
Tuliskan SQL Query untuk mendapatkan data berisi:
```
——————————————————————————————————
| ID | UserName | ParentUserName |
——————————————————————————————————
| 1 | Ali | Budi |
| 2 | Budi | NULL |
| 3 | Cecep | Ali |
——————————————————————————————————
```
*Kolom ParentUserName adalah UserName berdasarkan value Parent


## Solution
to start :
```
    make sol1
```

this solution running database MS SQL using docker,
and using [SQL Server Management Studio](https://aka.ms/ssmsfullsetup) to view or check seeded data

[SQL Query](sql/solution.sql) for this solution