package challenge1

import "fmt"

type Solution struct{}

func (s Solution) Run() error {

	//seed
	_, err := ExecSQL(`./challenge-1/sql/seed.sql`)
	if err != nil {
		return err
	}

	//select
	ret, err := ExecSelect(`./challenge-1/sql/solution.sql`)
	if err != nil {
		return err
	}
	fmt.Println(ret)

	return nil
}
