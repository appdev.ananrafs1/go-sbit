;WITH CTE_CONTAINER AS (
    SELECT
        ID,
        UserName,
        CAST(NULL AS VARCHAR(MAX)) [ParentUserName] 
    FROM
        dbo.[USER] U
    WHERE
        1=1 AND
        U.Parent = 0
    UNION ALL
    SELECT
        P.ID,
        P.UserName,
        C.UserName
    FROM
        dbo.[USER] P 
    INNER JOIN CTE_CONTAINER C on P.Parent = C.ID      
)

SELECT * FROM CTE_CONTAINER