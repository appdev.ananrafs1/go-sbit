package challenge1

import (
	"bytes"
	"database/sql"
	"fmt"
	"io"
	"os"

	_ "github.com/denisenkom/go-mssqldb"
)

var (
	server   = "localhost"
	port     = 1221
	user     = "sa"
	password = "Qwertyuiop[]"
	database = "GOSBIT"
)

func getConstring() string {
	return fmt.Sprintf("server=%s;user id=%s;password=%s;port=%d;database=%s;", server, user, password, port, database)
}

func ExecSQL(path string) (sql.Result, error) {
	fl, err := os.Open(path)
	if err != nil {
		fmt.Println(err, "1")
		return nil, err
	}
	defer fl.Close()
	bffer := new(bytes.Buffer)
	_, err = bffer.ReadFrom(fl)
	if err != nil {
		return nil, err
	}
	contents := bffer.String()
	db, err := sql.Open("mssql", getConstring())
	if err != nil {
		return nil, err
	}
	defer db.Close()
	res, err := db.Exec(contents)
	if err != nil && err != io.EOF {
		return nil, err
	}
	return res, nil
}

func ExecSelect(path string) ([][]string, error) {
	fl, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer fl.Close()
	bffer := new(bytes.Buffer)
	_, err = bffer.ReadFrom(fl)
	if err != nil {
		return nil, err
	}
	contents := bffer.String()
	db, err := sql.Open("mssql", getConstring())
	if err != nil {
		return nil, err
	}
	defer db.Close()
	res, err := db.Query(contents)
	if err != nil && err != io.EOF {
		return nil, err
	}
	ret := make([][]string, 0)
	defer res.Close()
	for res.Next() {
		rows := make([]string, 3)
		res.Scan(&rows[0], &rows[1], &rows[2])
		ret = append(ret, rows)
	}
	return ret, nil
}
