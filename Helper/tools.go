package tools

type SolutionRunner interface {
	Run() error
}

func Solve(sr SolutionRunner) error {
	return sr.Run()
}


