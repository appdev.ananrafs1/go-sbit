package challenge3

import (
	"fmt"
	"strings"
)

type Solution struct{}

func (s Solution) Run() error {
	fmt.Println(FindFirstStringInBracket("dis dus des ( uhys"))
	fmt.Println(FindFirstStringInBracketSolution("dis dus des ( uhys"))
	return nil
}

func FindFirstStringInBracket(str string) string {
	if len(str) > 0 {
		indexFirstBracketFound := strings.Index(str, "(")
		if indexFirstBracketFound >= 0 {
			runes := []rune(str)
			wordsAfterFirstBracket := string(runes[indexFirstBracketFound:len(str)])
			indexClosingBracketFound := strings.Index(wordsAfterFirstBracket, ")")
			if indexClosingBracketFound >= 0 {
				runes := []rune(wordsAfterFirstBracket)
				return string(runes[1 : indexClosingBracketFound-1])
			} else {
				return ""
			}
		} else {
			return ""
		}
	} else {
		return ""
	}
	return ""
}
