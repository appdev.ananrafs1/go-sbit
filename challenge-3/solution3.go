package challenge3

import (
	"fmt"
	"strings"
)

func FindFirstStringInBracketSolution(str string) string {
	var res string
	if len(str) < 1 {
		return res
	}
	OpenBracketIndex, CloseBracketIndex := strings.Index(str, "("), strings.Index(str, ")")
	if OpenBracketIndex == -1 || CloseBracketIndex == -1 {
		return res
	}
	fmt.Println(OpenBracketIndex, CloseBracketIndex)
	return string(str[OpenBracketIndex+1 : CloseBracketIndex-1]) //it should be `return string(str[OpenBracketIndex+1 : CloseBracketIndex-1])``
}
