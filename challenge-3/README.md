
# Challenge 3
Please refactor the code below to make it more concise, efficient and readable with good logic flow.

```
func findFirstStringInBracket(str string) string {
	if (len(str) > 0) {
		indexFirstBracketFound := strings.Index(str,"(")
		if indexFirstBracketFound >= 0 {
			runes := []rune(str)
			wordsAfterFirstBracket := string(runes[indexFirstBracketFound:len(str)])
			indexClosingBracketFound := strings.Index(wordsAfterFirstBracket,")")
			if indexClosingBracketFound >= 0 {
				runes := []rune(wordsAfterFirstBracket)
				return string(runes[1:indexClosingBracketFound-1]) //this one should be return string(runes[1:indexClosingBracketFound])
			}else{
				return ""
			}
		}else{
 			return ""
 		}
	}else{
		return ""
	}
	return ""
}
```


# Solution
[refactor](solution3.go)

run comparation table test :
```
	make sol3
```
