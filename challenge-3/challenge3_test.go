package challenge3_test

import (
	"fmt"
	"testing"

	challenge3 "appdev.ananrafs1/go-sbit/challenge-3"
	"github.com/google/go-cmp/cmp"
)

func TestFindFirstStringInBracketSolution(t *testing.T) {
	data := []string{"wadaw sekali (dag dig dug) waw", "dis dus des ( uhys", "ban bun ) supps", ""}
	for _, v := range data {
		t.Run(v, func(t *testing.T) {
			expected := challenge3.FindFirstStringInBracket(v)
			fmt.Println(expected)
			result := challenge3.FindFirstStringInBracketSolution(v)
			fmt.Println(result)
			if diff := cmp.Diff(expected, result); diff != "" {
				t.Error(diff)
			}
		})
	}

}
