# Challenge 2
Please write a microservice to search movies from http://www.omdbapi.com/
```
The microservice should be able to handle two transports : REST JSON HTTP and GRPC
Access credentials :
OMDBKey : xxx
URL : http://www.omdbapi.com/
```

* Example url call to search is --> GET http://www.omdbapi.com/?apikey=xxx&s=Batman&page=2

Functions to be implemented are :
```
- Search with pagination --> 2 parameters : "pagination" and "searchword"
- Get single detail of the movie
- Log each search calls to a dummy DB eg. let's just say we have a MySQL DB table for this.
```
Important aspects :
```
- Readability of code
- Good display on the knowledge of "Separation of Concerns for Codes"
- Write unit tests on some of the important files. (For Bigger plus points see below)
- Good use of asynchronousy with Go-Routine
```
Plus points:
```
- Implementation of Clean Architecture is a BIG plus
- Complete Unit tests on all codes
```


# Solution

template :
```
│   config.json
│   README.md
│
├───Delivery
│   ├───grpc
│   │   ├───client
│   │   │       main.go
│   │   │
│   │   ├───model
│   │   │       movie.pb.go
│   │   │       movie.proto
│   │   │       movie_grpc.pb.go
│   │   │
│   │   └───server
│   │           main.go
│   │
│   └───REST
│       │   main.go
│       │
│       └───Controller
│               moviesController.go
│               moviesController_test.go
│
├───Domain
│   │   logger.go
│   │   movie.go
│   │
│   └───mocks
│           LoggerRepository.go
│           LoggerUsecase.go
│           MovieService.go
│           MovieUsecase.go
│
├───Repository
│       loggerRepository.go
│       loggerRepository_test.go
│
├───Services
│       movieService.go
│       movieService_test.go
│
├───sql
│       Init.sql
│
└───UseCase
        loggerUseCase.go
        loggerUseCase_test.go
        movieUseCase.go
        movieUseCase_test.go
```



## REST API

to run :
```
	make sol2_rest omdbapi="[enter key here]"
```

## GRPC

to run :
	
	server :
```
		make sol2_grpc_server -omdbapi=[omdbkey]
```
	client :
```
		make sol2_grpc_client -operation=[0 - call search 'batman' page 2, 1 - find by id]
		e.g make sol2_grpc_client -operation=1
```