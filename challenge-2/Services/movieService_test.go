package services_test

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"

	domain "appdev.ananrafs1/go-sbit/challenge-2/Domain"
	"github.com/stretchr/testify/assert"
)

func TestSearch(t *testing.T) {
	mockReturnValue := domain.SearchMovie{
		Search: []domain.Movie{
			{
				Title: "batman",
				Year:  "2001",
			},
		},
	}
	testServer := httptest.NewServer(http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		str, err := json.Marshal(mockReturnValue)
		if err != nil {
			log.Fatal(err)
		}
		res.Write(str)
	}))
	defer func() { testServer.Close() }()
	req, err := http.NewRequest(http.MethodGet, testServer.URL, nil)
	if err != nil {
		log.Fatal(err)
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	var searchedObject domain.SearchMovie
	err = json.Unmarshal(data, &searchedObject)
	assert.NoError(t, err)

	assert.Equal(t, mockReturnValue, searchedObject)
}

func TestGetMovieDetail(t *testing.T) {
	mockReturnValue := domain.MovieDetail{
		Title: "batman",
		Year:  "2001",
	}

	testServer := httptest.NewServer(http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		str, err := json.Marshal(mockReturnValue)
		if err != nil {
			log.Fatal(err)
		}
		res.Write(str)
	}))
	defer func() { testServer.Close() }()
	req, err := http.NewRequest(http.MethodGet, testServer.URL, nil)
	if err != nil {
		log.Fatal(err)
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	var returnedObject domain.MovieDetail
	err = json.Unmarshal(data, &returnedObject)
	assert.NoError(t, err)
	assert.Equal(t, mockReturnValue, returnedObject)

}
