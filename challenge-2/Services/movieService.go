package services

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	domain "appdev.ananrafs1/go-sbit/challenge-2/Domain"
)

type movieService struct {
	omdbid string
}

func CreateMovieService(id string) domain.MovieService {
	return &movieService{omdbid: id}
}

func (s *movieService) Search(searchWord string, page int) (domain.SearchMovie, error) {
	url := fmt.Sprintf(`http://www.omdbapi.com/?apikey=%v&s=%v&page=%d`, s.omdbid, searchWord, page)
	resp, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	var searchedObject domain.SearchMovie
	err = json.Unmarshal(data, &searchedObject)
	if err != nil {
		log.Fatal(err)
	}
	return searchedObject, nil
}

func (s *movieService) GetMovieDetail(movieId string) (domain.MovieDetail, error) {
	url := fmt.Sprintf(`http://www.omdbapi.com/?apikey=%v&i=%v`, s.omdbid, movieId)
	resp, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	var returnedObject domain.MovieDetail
	err = json.Unmarshal(data, &returnedObject)
	if err != nil {
		log.Fatal(err)
	}
	return returnedObject, nil
}
