package repository_test

import (
	"context"
	"log"
	"testing"

	repo "appdev.ananrafs1/go-sbit/challenge-2/Repository"
	"github.com/stretchr/testify/assert"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

func TestStore(t *testing.T) {
	mockdb, mocker, err := sqlmock.New()
	if err != nil {
		log.Fatal(err)
	}
	log := `method=GET, uri=/, status=404, type=REST`
	query := `INSERT LOGGER SET Log=? , Created=CURRENT_TIMESTAMP()`
	mocker.ExpectPrepare(query).ExpectExec().WithArgs(log).WillReturnResult(sqlmock.NewResult(1, 1))

	r := repo.InitLoggerRepo(mockdb)
	err = r.Store(context.TODO(), log)

	assert.NoError(t, err)
}
