package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	domain "appdev.ananrafs1/go-sbit/challenge-2/Domain"
	_ "github.com/go-sql-driver/mysql"
)

type loggerRepo struct {
	DB *sql.DB
}

func CreateMySQL(host, port, user, pw, db string) *sql.DB {
	conString := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", user, pw, host, port, db)
	dbConn, err := sql.Open(`mysql`, conString)
	if err != nil {
		log.Fatal(err)
	}
	err = dbConn.Ping()
	if err != nil {
		log.Fatal(err)
	}

	return dbConn
}
func InitLoggerRepo(db *sql.DB) domain.LoggerRepository {
	return loggerRepo{DB: db}
}

func (l loggerRepo) Store(ctx context.Context, s string) error {
	query := `INSERT LOGGER SET Log=? , Created=CURRENT_TIMESTAMP()`
	stmt, err := l.DB.PrepareContext(ctx, query)
	if err != nil {
		return err
	}
	_, err = stmt.ExecContext(ctx, s)
	if err != nil {
		return err
	}
	return nil
}
