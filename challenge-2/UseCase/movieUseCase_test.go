package usecase_test

import (
	"errors"
	"testing"

	domain "appdev.ananrafs1/go-sbit/challenge-2/Domain"
	mck "appdev.ananrafs1/go-sbit/challenge-2/Domain/mocks"
	useCase "appdev.ananrafs1/go-sbit/challenge-2/UseCase"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestSearch(t *testing.T) {
	mockService := new(mck.MovieService)
	t.Run("Success", func(t *testing.T) {
		mockSearchMovie := domain.SearchMovie{
			Search: []domain.Movie{{
				Title: "AW",
			}},
		}

		mockService.On("Search", mock.Anything, mock.AnythingOfType("string"), mock.AnythingOfType("int")).Return(mockSearchMovie)

		svc := useCase.CreateMovieUsecase(mockService)
		r, _ := svc.Search("search random", 1)
		assert.Equal(t, mockSearchMovie, r)
	})
	t.Run("Error thrown by Service", func(t *testing.T) {
		err := errors.New("Error Thrown")
		mockService.On("Search", mock.Anything, mock.AnythingOfType("string"), mock.AnythingOfType("int")).Return(err)

		svc := useCase.CreateMovieUsecase(mockService)
		_, err = svc.Search("search random", 1)
		assert.EqualErrorf(t, err, "Error Thrown", err.Error())
	})

}

func TestGetMovieDetail(t *testing.T) {
	mockMovieService := new(mck.MovieService)
	t.Run("Success", func(t *testing.T) {
		mockMovieDetail := domain.MovieDetail{
			Title: "testing",
		}
		mockMovieService.On("GetMovieDetail", mock.Anything, mock.AnythingOfType("string")).Return(mockMovieDetail)

		svc := useCase.CreateMovieUsecase(mockMovieService)
		actual, _ := svc.GetDetail("random")
		assert.Equal(t, actual, mockMovieDetail)
	})

	t.Run("Error thrown", func(t *testing.T) {
		errorThrown := "error from Services"
		mockMovieService.On("GetMovieDetail", mock.Anything, mock.AnythingOfType("string")).Return(errors.New(errorThrown))

		svc := useCase.CreateMovieUsecase(mockMovieService)
		_, err := svc.GetDetail("random")
		assert.EqualErrorf(t, err, errorThrown, err.Error())
	})
}
