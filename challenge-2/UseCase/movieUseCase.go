package usecase

import (
	domain "appdev.ananrafs1/go-sbit/challenge-2/Domain"
)

type movieUsecase struct {
	service domain.MovieService
}

func CreateMovieUsecase(sv domain.MovieService) domain.MovieUsecase {
	return &movieUsecase{service: sv}
}

func (m *movieUsecase) Search(s string, p int) (domain.SearchMovie, error) {
	return m.service.Search(s, p)
}
func (m *movieUsecase) GetDetail(id string) (domain.MovieDetail, error) {
	return m.service.GetMovieDetail(id)
}
