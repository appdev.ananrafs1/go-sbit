package usecase

import (
	"context"

	domain "appdev.ananrafs1/go-sbit/challenge-2/Domain"
)

type loggerUsecase struct {
	repo domain.LoggerRepository
}

func CreateLoggerUsecase(r domain.LoggerRepository) domain.LoggerUsecase {
	return loggerUsecase{repo: r}
}

func (l loggerUsecase) Write(p []byte) (n int, err error) {
	ctx := context.Background()
	err = l.repo.Store(ctx, string(p))
	return 1, err
}
