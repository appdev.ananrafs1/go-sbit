package usecase_test

import (
	"errors"
	"testing"

	mck "appdev.ananrafs1/go-sbit/challenge-2/Domain/mocks"
	useCase "appdev.ananrafs1/go-sbit/challenge-2/UseCase"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestWrite(t *testing.T) {
	mockLoggerRepo := new(mck.LoggerRepository)

	t.Run("Succes", func(t *testing.T) {

		mockLoggerRepo.On("Store", mock.Anything, mock.AnythingOfType("string")).Return(nil)

		actual := useCase.CreateLoggerUsecase(mockLoggerRepo)
		ret, _ := actual.Write([]byte("hy"))
		assert.Equal(t, ret, 1)
	})

	t.Run("Error from Repo", func(t *testing.T) {
		errorThrown := `Error from Repo`
		mockLoggerRepo.On("Store", mock.Anything, mock.AnythingOfType("string")).Return(errors.New(errorThrown))

		actual := useCase.CreateLoggerUsecase(mockLoggerRepo)
		_, err := actual.Write([]byte("hy"))
		assert.EqualErrorf(t, err, errorThrown, err.Error())
	})
}
