package controller

import (
	"log"
	"net/http"

	domain "appdev.ananrafs1/go-sbit/challenge-2/Domain"
	"github.com/labstack/echo/v4"
)

type MoviesController struct {
	useCase domain.MovieUsecase
}

func InitMovieController(uc domain.MovieUsecase) MoviesController {
	return MoviesController{useCase: uc}
}

func (ctrl MoviesController) Search(c echo.Context) error {
	qs := struct {
		Search string `query:"s"`
		Page   int    `query:"p"`
	}{}
	err := c.Bind(&qs)
	if err != nil {
		log.Fatal(err)
	}
	res, err := ctrl.useCase.Search(qs.Search, qs.Page)
	if err != nil {
		log.Fatal(err)
	}
	return c.JSON(http.StatusOK, res.Search)
}

func (ctrl MoviesController) GetById(c echo.Context) error {

	res, err := ctrl.useCase.GetDetail(c.Param("id"))
	if err != nil {
		log.Fatal(err)
	}
	return c.JSON(http.StatusOK, res)
}
