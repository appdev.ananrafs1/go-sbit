package controller_test

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	ctrlr "appdev.ananrafs1/go-sbit/challenge-2/Delivery/REST/Controller"
	domain "appdev.ananrafs1/go-sbit/challenge-2/Domain"
	"appdev.ananrafs1/go-sbit/challenge-2/Domain/mocks"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestSearch(t *testing.T) {
	mockUseCase := new(mocks.MovieUsecase)
	mockReturn := domain.SearchMovie{
		Search: []domain.Movie{
			{
				Title: "batman",
				Year:  "2021",
			},
		},
	}
	mockUseCase.On("Search", mock.AnythingOfType("string"), mock.AnythingOfType("int")).Return(mockReturn)

	qParam := make(url.Values)
	qParam.Set("s", "batman")
	qParam.Set("p", "2")
	testreq := httptest.NewRequest(http.MethodGet, "/movie?"+qParam.Encode(), nil)
	recorder := httptest.NewRecorder()
	c := echo.New().NewContext(testreq, recorder)

	h := ctrlr.InitMovieController(mockUseCase)
	h.Search(c)
	assert.Equal(t, mockReturn.Search, recorder.Body)
}
func TestGetById(t *testing.T) {
	mockUseCase := new(mocks.MovieUsecase)
	mockReturn := domain.MovieDetail{
		Title: "batman",
		Genre: "Horor",
	}
	mockUseCase.On("GetDetail", mock.AnythingOfType("string")).Return(mockReturn)

	testreq := httptest.NewRequest(http.MethodGet, "/", nil)
	recorder := httptest.NewRecorder()
	c := echo.New().NewContext(testreq, recorder)
	c.SetPath("/movie/:id")
	c.SetParamNames("id")
	c.SetParamValues("ttjdamwu12")

	h := ctrlr.InitMovieController(mockUseCase)
	h.GetById(c)
	assert.Equal(t, mockReturn, recorder.Body)
}
