package main

import (
	"flag"
	"log"
	"net/http"

	ctrlr "appdev.ananrafs1/go-sbit/challenge-2/Delivery/REST/Controller"
	repo "appdev.ananrafs1/go-sbit/challenge-2/Repository"
	services "appdev.ananrafs1/go-sbit/challenge-2/Services"
	useCase "appdev.ananrafs1/go-sbit/challenge-2/UseCase"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/spf13/viper"
)

func main() {
	omdbapi := flag.String("omdbapi", "no", "omdb id")
	flag.Parse()
	e := echo.New()
	lRepo := repo.InitLoggerRepo(repo.CreateMySQL(
		viper.GetString(`database.host`),
		viper.GetString(`database.port`),
		viper.GetString(`database.user`),
		viper.GetString(`database.pass`),
		viper.GetString(`database.name`),
	))
	lUsecase := useCase.CreateLoggerUsecase(lRepo)
	movieSvc := services.CreateMovieService(*omdbapi)
	movieUsecase := useCase.CreateMovieUsecase(movieSvc)
	movieController := ctrlr.InitMovieController(movieUsecase)
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Output: lUsecase,
		Format: "method=${method}, uri=${uri}, status=${status}, type=REST\n",
	}))
	e.Use(middleware.Recover())
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})
	e.GET("/movie", func(c echo.Context) error {
		return movieController.Search(c)
	})
	e.GET("/movie/:id", func(c echo.Context) error {
		return movieController.GetById(c)
	})

	e.Start(":8081")

}

func init() {
	viper.SetConfigFile(`./challenge-2/config.json`)
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal(err)
	}
}
