package main

import (
	"context"
	"flag"
	"fmt"
	"log"

	"appdev.ananrafs1/go-sbit/challenge-2/Delivery/grpc/model"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
)

func init() {
	viper.SetConfigFile(`./challenge-2/config.json`)
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	dialer, err := grpc.Dial(fmt.Sprintf("localhost%v", viper.GetString("server.address")), grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	defer dialer.Close()

	cl := model.NewMovieGrpcClient(dialer)

	operation := flag.Int("operation", 0, "to call grpc, [search | getdetail")
	fmt.Println(*operation)
	switch *operation {
	case 0:
		res, err := cl.Search(context.Background(), &model.SearchMovieRequest{SearchWord: `batman`, Page: 2})
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(res)
	case 1:
		res, err := cl.GetById(context.Background(), &model.DetailRequest{Id: "dawds"})
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(res)

	default:
		fmt.Println("not supported yet")
	}

}
