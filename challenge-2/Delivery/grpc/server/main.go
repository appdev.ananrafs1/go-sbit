package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net"

	"appdev.ananrafs1/go-sbit/challenge-2/Delivery/grpc/model"
	domain "appdev.ananrafs1/go-sbit/challenge-2/Domain"
	repo "appdev.ananrafs1/go-sbit/challenge-2/Repository"
	services "appdev.ananrafs1/go-sbit/challenge-2/Services"
	useCase "appdev.ananrafs1/go-sbit/challenge-2/UseCase"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
)

func init() {
	viper.SetConfigFile(`./challenge-2/config.json`)
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal(err)
	}
}

func main() {

	listener, err := net.Listen("tcp", fmt.Sprintf("localhost%v", viper.GetString("server.address")))
	if err != nil {
		log.Fatal("Failed to Listen, ", err)
	}
	defer listener.Close()
	s := grpc.NewServer()
	model.RegisterMovieGrpcServer(s, CreateServer())
	err = s.Serve(listener)
	if err != nil {
		log.Fatal("Server failed to run, ", err)
	}

}

type server struct {
	model.UnimplementedMovieGrpcServer
	logger domain.LoggerUsecase
	movie  domain.MovieUsecase
}

func CreateServer() *server {
	omdbapi := flag.String("omdbapi", "no", "omdb id")
	flag.Parse()
	lRepo := repo.InitLoggerRepo(repo.CreateMySQL(
		viper.GetString(`database.host`),
		viper.GetString(`database.port`),
		viper.GetString(`database.user`),
		viper.GetString(`database.pass`),
		viper.GetString(`database.name`),
	))
	lUsecase := useCase.CreateLoggerUsecase(lRepo)
	movieSvc := services.CreateMovieService(*omdbapi)
	movieUsecase := useCase.CreateMovieUsecase(movieSvc)
	return &server{logger: lUsecase, movie: movieUsecase}
}

func (srv *server) Search(ctx context.Context, req *model.SearchMovieRequest) (*model.SearchMovieResponse, error) {
	var (
		resp model.SearchMovieResponse
		err  error
	)
	defer func() {
		loggInfo := fmt.Sprintf(`method=Search, uri=%v, status=%v, type=grpc`, req, resp)
		srv.logger.Write([]byte(loggInfo))
	}()
	movies, err := srv.movie.Search(req.SearchWord, int(req.Page))
	resp = model.SearchMovieResponse{}

	for _, v := range movies.Search {
		resp.Search = append(resp.Search, &model.Movie{
			Title:  v.Title,
			Year:   v.Year,
			ImdbID: v.ImdbID,
			Type:   v.Type,
			Poster: v.Poster,
		})
	}
	return &resp, err
}

func (srv *server) GetById(ctx context.Context, req *model.DetailRequest) (*model.MovieDetailResponse, error) {
	var (
		resp model.MovieDetailResponse
		err  error
	)
	defer func() {
		loggInfo := fmt.Sprintf(`method=Search, uri=%v, status=%v, type=grpc`, req, resp)
		srv.logger.Write([]byte(loggInfo))
	}()

	movieDetail, err := srv.movie.GetDetail(req.Id)

	resp.Title = movieDetail.Title
	resp.Year = movieDetail.Year
	resp.Rated = movieDetail.Rated
	resp.Released = movieDetail.Released
	resp.Runtime = movieDetail.Runtime
	resp.Genre = movieDetail.Genre
	resp.Director = movieDetail.Director
	resp.Writer = movieDetail.Writer
	resp.Actors = movieDetail.Actors
	resp.Plot = movieDetail.Plot
	resp.Language = movieDetail.Language
	resp.Country = movieDetail.Country
	resp.Awards = movieDetail.Awards
	resp.Poster = movieDetail.Poster
	resp.Metascore = movieDetail.Metascore
	resp.ImdbRating = movieDetail.ImdbRating
	resp.ImdbVotes = movieDetail.ImdbVotes
	resp.ImdbID = movieDetail.ImdbID
	resp.Type = movieDetail.Type
	resp.DVD = movieDetail.DVD
	resp.BoxOffice = movieDetail.BoxOffice
	resp.Production = movieDetail.Production
	resp.Website = movieDetail.Website
	resp.Response = movieDetail.Response
	for _, v := range movieDetail.Ratings {
		resp.Ratings = append(resp.Ratings, &model.Rating{Source: v.Source, Value: v.Value})
	}
	return &resp, err
}
