package domain

import "context"

type Logger struct {
}

type LoggerUsecase interface {
	Write(p []byte) (n int, err error)
}

type LoggerRepository interface {
	Store(ctx context.Context, s string) error
}
