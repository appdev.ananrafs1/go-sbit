package main

import (
	"flag"
	"log"

	tools "appdev.ananrafs1/go-sbit/Helper"
	challenge1 "appdev.ananrafs1/go-sbit/challenge-1"
	challenge3 "appdev.ananrafs1/go-sbit/challenge-3"
	challenge4 "appdev.ananrafs1/go-sbit/challenge-4"
)

func main() {
	sol := flag.Int("sol", 1, "Challenge to solve")
	flag.Parse()
	mapSolver := map[int]tools.SolutionRunner{
		1: challenge1.Solution{},
		4: challenge4.Solution{},
		3: challenge3.Solution{},
	}
	err := mapSolver[*sol].Run()
	if err != nil {
		log.Fatalf("Error : %v", err)
	}

}
